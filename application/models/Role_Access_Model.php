<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_Access_Model extends CI_Model{

	private $table = "role_access";
	private $primary = "id_role_access";
	
	function read(){
		$this->db->join('role_user','role_user.id_role_user = role_access.id_role_user','INNER');
		$this->db->join('user','user.id_user = role_access.id_user','INNER');
		return $this->db->get($this->table)->result();
	}
	function getByUser($id){
		$this->db->where('id_user',$id);
		$this->db->join('user','user.id_user = role_access.id_user','INNER');
		return $this->db->get($this->table)->result();
	}
	function create($data){
		$this->db->insert($this->table,$data);
	}
	function delete($data,$id){
		$this->db->where($this->primary,$id);
		$this->db->delete($this->table,$data);
	}

}