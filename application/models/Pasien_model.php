<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasien_model extends CI_Model{

	private $table = "mst_pasien";
	private $primary = "id_mst_pasien";

	function getByID($id){
		$query = "call getPasienById('".$id."')";
		$hasil = $this->db->query($query);
		mysqli_next_result($this->db->conn_id);
		if ($hasil->num_rows() > 0) {
			return $hasil->result();
		}
	}
	function getDiagnosa($kode){
		$query = "call getDiagnosa('".$kode."')";
		$sql_query = $this->db->query($query);
	    mysqli_next_result( $this->db->conn_id);
	    if($sql_query->num_rows()==1){
	        return $sql_query->row();
	    }

	}
	function read(){
		$query = "call getPasien()";
		$hasil = $this->db->query($query);
		mysqli_next_result($this->db->conn_id);
		if ($hasil->num_rows() > 0) {
			return $hasil->result();
		}
	}
	function create($data){
		$this->db->insert($this->table,$data);
	}
	function update($post,$id){
		$this->db->where($this->primary,$id);
		$this->db->update($this->table,$post);
	}
	function delete($id){
		$this->db->where($this->primary,$id);
		$this->db->delete($this->table);
	}
	function noRm(){
		$this->db->select('RIGHT(mst_pasien.no_rm,7)as kode',FALSE);
		$this->db->order_by('no_rm','DESC');
		$this->db->limit(1);
		$query = $this->db->get('mst_pasien');
		if ($query->num_rows()<>0) {
			$data = $query->row();
			$kode = intval($data->kode) + 1;

		}else{
			$kode = 1;
		}
		$kodemax = str_pad($kode,7,"000",STR_PAD_LEFT);
		$kodejadi = 'RM'.$kodemax;
		return $kodejadi;
	}


}
