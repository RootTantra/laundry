<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class userModel extends CI_Model
{
	private $table = "user";
	private $primary = "id_user";

	public function cek_user($data) {
		$query = $this->db->get_where($this->table, $data);
		return $query;
	}
	function read(){
		return $this->db->get($this->table)->result();
	}
	function create($data){
		$this->db->insert($this->table,$data);
	}
	function update($data,$id){
		$this->db->where($this->primary,$id);
		$this->db->update($this->table,$data);
	}
	function getByID($id){
		$this->db->where($this->primary,$id);
		return $this->db->get($this->table)->result();
	}
	function delete($data,$id){
		$this->db->where($this->primary,$id);
		$this->db->delete($this->table,$data);

	}
}