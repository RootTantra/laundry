<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penjualan_model extends CI_Model{

	private $table = "penjualan";
	private $primary = "faktur_penjualan";

	function read(){
		$this->db->select('penjualan.*,nama_pegawai');
		$this->db->join('mst_pegawai','mst_pegawai.id_mst_pegawai = penjualan.id_mst_pegawai','INNER');
		return $this->db->get($this->table)->result();
	}
	function getById($id){
		$this->db->select('penjualan.*,nama_pegawai');
		$this->db->where($this->primary,$id);
		$this->db->join('mst_pegawai','mst_pegawai.id_mst_pegawai = penjualan.id_mst_pegawai','INNER');
		return $this->db->get($this->table)->result();
	}
	function getoneDay($date){
		$this->db->select('penjualan.*,nama_pegawai');
		$this->db->where('penjualan.tgl_penjualan',$date);
		$this->db->join('mst_pegawai','mst_pegawai.id_mst_pegawai = penjualan.id_mst_pegawai','INNER');
		return $this->db->get($this->table)->result();
	}
	function create($data){
		$this->db->insert($this->table,$data);
	}
	function update($post,$id){
		$this->db->where($this->primary,$id);
		$this->db->update($this->table,$post);
	}
	function delete($id){
		$this->db->where($this->primary,$id);
		$this->db->delete($this->table);
	}

	function NoFaktur(){
		$this->db->select('RIGHT(penjualan.faktur_penjualan,5)as kode',FALSE);
		$this->db->order_by('faktur_penjualan','DESC');
		$this->db->limit(1);
		$query = $this->db->get('penjualan');
		if ($query->num_rows()<>0) {
			$data = $query->row();
			$kode = intval($data->kode) + 1;

		}else{
			$kode = 1;
		}
		$kodemax = str_pad($kode,5,"000",STR_PAD_LEFT);
		$kodejadi = 'FP'.$kodemax;
		return $kodejadi;
	}

	function getTotal($id){
		$this->db->select('total');
		$this->db->where($this->primary,$id);
		$query = $this->db->get($this->table);
		$hasil = $query->row();
		return $hasil->total;
	}

	function LaporanLunas($awal,$akhir){
  		$query = "call laporanPenjualanLunas('".$awal."','".$akhir."')";
  		$hasil = $this->db->query($query);
  		mysqli_next_result($this->db->conn_id);
  		if ($hasil->num_rows() > 0) {
  			return $hasil->result();
  		}
	}
	function LaporanBLunas($awal,$akhir){
  		$query = "call laporanPenjualanBelumLunas('".$awal."','".$akhir."')";
  		$hasil = $this->db->query($query);
  		mysqli_next_result($this->db->conn_id);
  		if ($hasil->num_rows() > 0) {
  			return $hasil->result();
  		}
	}
	function LaporanAll($awal,$akhir){
  		$query = "call laporanPenjualanAll('".$awal."','".$akhir."')";
  		$hasil = $this->db->query($query);
  		mysqli_next_result($this->db->conn_id);
  		if ($hasil->num_rows() > 0) {
  			return $hasil->result();
  		}
	}

}
