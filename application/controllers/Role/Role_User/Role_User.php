<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_User extends CI_Controller {


    public function __construct() {
        parent::__construct();

        $this->load->library('Template','template');
        $this->load->model('Role_User_Model','role_user');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Jakarta");

    }
    public function index(){
        $data['role_user'] = $this->role_user->read();
        $this->template->layout('Role/Role_User/table',$data);
    }
    public function create_role_user(){
        if (count($this->input->post()) > 0) {
            $post = $this->input->post();
            $this->db->trans_start();
                $this->role_user->create($post);
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                   $this->session->set_flashdata("pesan_eror","<div class='alert alert-danger alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                            Gagal Menyimpan Data !
                    </div>");
                   redirect('Role/Role_User/Role_User','refresh');
            }else{
                   $this->session->set_flashdata("pesan_eror","<div class='alert bg-blue alert-dismissible' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                               Succes Menyimpan Data !.
                            </div>");
                   redirect('Role/Role_User/Role_User','refresh');
            }
        }
        $this->template->layout('Role/Role_User/create');
    }
    public function update_role_user($id){
        if (count($this->input->post()) > 0) {
            $post = $this->input->post();
            $this->db->trans_start();
                $post['id_role_user'] = $id;
                $this->role_user->update($post,$id);
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                   $this->session->set_flashdata("pesan_eror","<div class='alert alert-danger alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                            Gagal Update Data !
                    </div>");
                   redirect('Role/Role_User/Role_User','refresh');
            }else{
                   $this->session->set_flashdata("pesan_eror","<div class='alert bg-blue alert-dismissible' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                               Succes Update Data !.
                            </div>");
                   redirect('Role/Role_User/Role_User','refresh');
            }
        }
        $data['role_user'] = $this->role_user->getByID($id);
        $this->template->layout('Role/Role_User/update',$data);
    }
    public function delete_role_user($id){
        $this->db->trans_start();
            $post['id_role_user'] = $id;
            $this->role_user->delete($post,$id);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
               $this->session->set_flashdata("pesan_eror","<div class='alert alert-danger alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                        Gagal Delete Data !
                </div>");
               redirect('Role/Role_User/Role_User','refresh');
        }else{
               $this->session->set_flashdata("pesan_eror","<div class='alert bg-blue alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                           Succes Delete Data !.
                        </div>");
               redirect('Role/Role_User/Role_User','refresh');
        }
    }

}