<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_Access extends CI_Controller {


    public function __construct() {
        parent::__construct();
        
        $this->load->library('Template','template');
        $this->load->model('Role_Access_Model','role_access');
        $this->load->model('Role_User_Model','role_user');
        $this->load->model('userModel','user');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Jakarta");

    }
    public function index(){
            $data['role_access'] = $this->role_access->read();
            $this->template->layout('Role/Role_Access/table',$data);
        }
        public function create(){
            if (count($this->input->post()) > 0) {
                $post = $this->input->post();
                $this->db->trans_start();
                    $this->role_access->create($post);
                $this->db->trans_complete();
                if ($this->db->trans_status() === false) {
                       $this->session->set_flashdata("pesan_eror","<div class='alert alert-danger alert-dismissible'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                                Gagal Menyimpan Data !
                        </div>");
                       redirect('Role/Role_Access/Role_Access','refresh');
                }else{
                       $this->session->set_flashdata("pesan_eror","<div class='alert bg-blue alert-dismissible' role='alert'>
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                    <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                                   Succes Menyimpan Data !.
                                </div>");
                       redirect('Role/Role_Access/Role_Access','refresh');
                }
            }
            $data['role_user'] = $this->role_user->read();
            $data['user'] = $this->user->read();
            $this->template->layout('Role/Role_Access/create',$data);
        }
        public function delete($id){
        $this->db->trans_start();
            $post['id_role_access'] = $id;
            $this->role_access->delete($post,$id);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
               $this->session->set_flashdata("pesan_eror","<div class='alert alert-danger alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                        Gagal Delete Data !
                </div>");
               redirect('Role/Role_Access/Role_Access','refresh');
        }else{
               $this->session->set_flashdata("pesan_eror","<div class='alert bg-blue alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                           Succes Delete Data !.
                        </div>");
               redirect('Role/Role_Access/Role_Access','refresh');
        }
    }

}