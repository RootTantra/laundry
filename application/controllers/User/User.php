<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('Template','template');
        $this->load->model('userModel','user');
        $this->load->helper('date');
        $this->load->helper('tanggal');
        date_default_timezone_set("Asia/Jakarta");

    }
    public function index(){
        $data['data_user'] = $this->user->read();
        $this->template->layout('User/table',$data);
    }
    public function create(){
        if (count($this->input->post()) > 0) {
            $post = $this->input->post();
            $post['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
            $data_menu = array(
                    'first_name' => $post['first_name'],
                    'last_name' => $post['last_name'],
                    'username' => $post['username'],
                    'email' => $post['email'],
                    'password' => $post['password'],
                    'no_telp' => $post['no_telp'],
                    'foto' => $_FILES['foto']['name']
            );
            $this->db->trans_start();
              $config['upload_path'] = './assets/img/';    
              $config['allowed_types'] = 'jpg|png|jpeg|PNG';    
              $config['max_size']  = '2048';    
              $config['remove_space'] = TRUE;      
              $this->load->library('upload', $config); 
               if ($this->upload->do_upload('foto')) {
                $this->user->create($data_menu);
                }
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                   $this->session->set_flashdata("pesan_eror","<div class='alert alert-danger alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                            Gagal Menyimpan Data !
                    </div>");
           redirect('User/User','refresh');
            }else{
                   $this->session->set_flashdata("pesan_eror","<div class='alert bg-blue alert-dismissible' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                               Succes Menyimpan Data !.
                            </div>");
                   redirect('User/User','refresh');
            }
        }
        $this->template->layout('User/create');
    }
    public function update($id){
        if (count($this->input->post()) > 0) {
            $post = $this->input->post();
            $post['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
            $data_menu = array(
                    'first_name' => $post['first_name'],
                    'last_name' => $post['last_name'],
                    'username' => $post['username'],
                    'email' => $post['email'],
                    'password' => $post['password'],
                    'no_telp' => $post['no_telp'],
                    'foto' => $_FILES['foto']['name']
            );
            $this->db->trans_start();
                $post['id_user'] = $id;
                  $this->user->update($data_menu,$id);
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                   $this->session->set_flashdata("pesan_eror","<div class='alert alert-danger alert-dismissible'>
                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                            <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                            Gagal Update Data !
                    </div>");
           redirect('User/User','refresh');
            }else{
                   $this->session->set_flashdata("pesan_eror","<div class='alert bg-blue alert-dismissible' role='alert'>
                                <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                                <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                               Succes Update Data !.
                            </div>");
                   redirect('User/User','refresh');
            }
        }
        $data['data_user'] = $this->user->getByID($id);
        $this->template->layout('User/update',$data);
    }
    public function delete($id){
        $this->db->trans_start();
            $post['id_user'] = $id;
            $this->user->delete($post,$id);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
               $this->session->set_flashdata("pesan_eror","<div class='alert alert-danger alert-dismissible'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                        Gagal Delete Data !
                </div>");
       redirect('User/User','refresh');
        }else{
               $this->session->set_flashdata("pesan_eror","<div class='alert bg-blue alert-dismissible' role='alert'>
                            <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                            <h4><i class='icon fa fa-ban'></i> Information !!!</h4>
                           Succes Delete Data !.
                        </div>");
               redirect('User/User','refresh');
        }
    }
  }