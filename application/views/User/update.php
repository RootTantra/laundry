
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12 col-md-12">
          <div class="box">
            <div class="box-header">
            <?php echo $this->session->flashdata('pesan_eror'); ?>
              <h2>Update User
              </h2>
            </div>
            <div class="box-body">
                <div class="form">
                  <div class="col-md-6 col-md-offset-3">
                    <?php echo form_open("", array('enctype'=>'multipart/form-data')); ?>
                    <?php foreach ($data_user as $key => $row): ?>
                      <div class="form-group">
                        <label class="control-label">First Name : </label>
                        <input type="text" name="first_name" class="form-control" placeholder="First Name" value="<?= $row->first_name; ?>" required>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Last Name : </label>
                        <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?= $row->last_name; ?>" required>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Username : </label>
                        <input type="text" name="username" class="form-control" placeholder="Username"  value="<?= $row->username; ?>" required>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Email : </label>
                        <input type="text" name="email" class="form-control" placeholder="E-Mail" value="<?= $row->email; ?>" required>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Password : </label>
                        <input type="password" name="password" class="form-control" placeholder="Password" value="<?= $row->password; ?>" required>
                      </div>
                      <div class="form-group">
                        <label class="control-label">No Telp : </label>
                        <input type="text" name="no_telp" class="form-control" placeholder="No Telp" value="<?= $row->no_telp; ?>" required>
                      </div>
                      <div class="form-group">
                        <label class="control-label">Foto : </label>
                        <input type="file" name="foto" class="form-control"
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Simpan">Simpan</button>
                        <a class="btn btn-danger" href="<?= base_url();?>User/User" data-toggle="tooltip" data-placement="top" title="Kembali">Kembali</a>
                      </div>
                      <?php endforeach ?>
                    <?= form_close();?>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<script>
  $(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
  $('#datepicker').datetimepicker({
        format:'Y-m-d',
        mask:false,
        timepicker:false,
        minDate: '-2015/01/01'
    });
</script>