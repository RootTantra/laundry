 <link rel="stylesheet" href="<?= base_url();?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
 
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12 col-md-12">
          <div class="box">
            <div class="box-header"> 
              <?php echo $this->session->flashdata('pesan_eror'); ?>
              <h2>Data User
                <a href="<?php echo base_url() ?>User/User/create" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Tambah User"><i class="fa fa-plus"></i> Tambah</a> 
              </h2>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover" id="table">
                <thead>
                  <tr>
                    <th>NO </th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>No Telp</th>
                    <th>Foto</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($data_user as $key => $row): ?>
                    <tr>
                      <td><?= $key + 1;?></td>
                      <td><?= $row->first_name;?></td>
                      <td><?= $row->last_name;?></td>
                      <td><?= $row->no_telp;?></td>
                      <td><?= "<img src='".base_url("assets/img/".$row->foto)."'width='100' height='100'";?></td>
                      <td>
                        <a href="<?= base_url();?>User/User/update/<?= $row->id_user;?>" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Edit <?= $row->first_name;?>"><i class="fa fa-pencil"></i> Edit</a>
                        <a href="<?= base_url();?>User/User/delete/<?= $row->id_user;?>" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Edit <?= $row->first_name;?>" onclick="return ConfirmDialog()"> <i class="fa fa-trash"></i> Delete</a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<script src="<?= base_url();?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#table').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    })
  });

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });

  function ConfirmDialog() {
  var x=confirm("Apakah anda yakin ingin menghapus data ini?")
  if (x) {
    return true;
  } else {
    return false;
  }
}
</script>